package com.usatoday.gannett.subscriber.ejb.session;
import java.util.Collection;

import javax.ejb.Remote;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;

@Remote
public interface SubscriberTransXBeanRemote {
	public Collection<ExtranetSubscriberTransactionIntf> fetchTransactionsInBatch(String batchID) throws UsatException;
	public int getCountOfNewTransactions() throws UsatException;
	public Collection<ExtranetSubscriberTransactionIntf> getNewTransactionsForProcessing() throws UsatException;
	public int setTransactionStateOfBatchToReProcessed(String batchID) throws UsatException;
	public int setTransactionStateFromProcessingToProcessed() throws UsatException;
	public int purgeTransactionsOlderThan(int numDaysBeforePurge) throws UsatException;
}
