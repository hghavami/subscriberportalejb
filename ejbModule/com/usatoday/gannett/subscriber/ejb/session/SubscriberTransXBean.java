package com.usatoday.gannett.subscriber.ejb.session;

import java.util.Collection;

import javax.annotation.security.PermitAll;
import javax.ejb.Stateless;

import com.usatoday.UsatException;
import com.usatoday.business.interfaces.subscriptionTransactions.ExtranetSubscriberTransactionIntf;
import com.usatoday.businessObjects.subscriptionTransactions.SubscriberTransactionBatchBO;

/**
 * Session Bean implementation class SubscriberTransXBean
 */
@Stateless
@PermitAll
public class SubscriberTransXBean implements SubscriberTransXBeanRemote {

    /**
     * Default constructor. 
     */
    public SubscriberTransXBean() {
       
    }
    
    
	@Override
	public int getCountOfNewTransactions() throws UsatException {
		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();

		int numReady = 0;
		
		numReady = transBO.getCountOfNewTransactions();
		
		return numReady;
	}

	@Override
	public Collection<ExtranetSubscriberTransactionIntf> getNewTransactionsForProcessing()
			throws UsatException {
		Collection<ExtranetSubscriberTransactionIntf> transactions = null;
		
		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();
		
		transactions = transBO.getNewTransactionsForProcessing();
		
		return transactions;
	}

	@Override
	public int setTransactionStateOfBatchToReProcessed(String batchID)
			throws UsatException {

		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();
		
		int numUpdated = transBO.setTransactionStateOfBatchToReProcessed(batchID);
		
		return numUpdated;
	}

	@Override
	public int setTransactionStateFromProcessingToProcessed()
			throws UsatException {
		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();

		int numUpdated = 0;
		
		numUpdated = transBO.setTransactionStateFromProcessingToProcessed();
		
		return numUpdated;
	}

	@Override
	public int purgeTransactionsOlderThan(int numDaysBeforePurge)
			throws UsatException {

		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();
		
		int numDeleted = 0;
		
		numDeleted = transBO.purgeTransactionsOlderThan(numDaysBeforePurge);			
		
		return numDeleted;
	}


	@Override
	public Collection<ExtranetSubscriberTransactionIntf> fetchTransactionsInBatch(
			String batchID) throws UsatException {
		Collection<ExtranetSubscriberTransactionIntf> transactions = null;
		
		SubscriberTransactionBatchBO transBO = new SubscriberTransactionBatchBO();
		
		transactions = transBO.getTransactionsInBatch(batchID);
		
		return transactions;
	}
}
